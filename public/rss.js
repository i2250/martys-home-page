const DOMPARSER = new DOMParser().parseFromString.bind(new DOMParser())
/* Fetch RSSs from JSON */
fetch('rss.json').then((res) => {
	res.text().then((data) => {
		var frag = document.createDocumentFragment()
		var hasBegun = true
		JSON.parse(data).urls.forEach((u) => {
			try {
				var url = new URL(u)
			}
			catch (e) {
				console.error('URL invalid');
				return
			}
			/* Fetch the RSS Feed */
			fetch(url).then((res) => {
				res.text().then((xmlTxt) => {
					/* Parse the RSS Feed and display the content */
					try {
						let doc = DOMPARSER(xmlTxt, "text/xml");
						let section = document.createElement('section');
						section.classList.add('feed');
						let heading = document.createElement('h1');
						heading.textContent = url.pathname;
						specificClassNames = url.pathname.split("/");
						value = specificClassNames[2].split(".");
						console.log(value[0]);
						section.classList.add("A" + value[0]);
						frag.appendChild(section)
						section.appendChild(heading)
						doc.querySelectorAll('item').forEach((item) => {
							let temp = document.importNode(document.querySelector('template').content, true);
							let i = item.querySelector.bind(item)
							let t = temp.querySelector.bind(temp)
							t('h2').textContent = !!i('title') ? i('title').textContent : '-'
							t('a').textContent = t('a').href = !!i('link') ? i('link').textContent : '#'
							t('p').innerHTML = !!i('description') ? i('description').textContent : '-'
							t('h3').textContent = url.hostname
							section.appendChild(temp)
						})
					} catch (e) {
						console.error('Error in parsing the feed')
					}
					document.querySelector('output').appendChild(frag)
				})
			}).catch(() => console.error('Error in fetching the RSS feed'))
		})
	})
}).catch(() => console.error('Error in fetching the URLs json'))
